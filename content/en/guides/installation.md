---
title: Advanced installation options
linkTitle: Installation
date: 2023-11-23
description: Requiremenst an option install the SDaaS platform in your environment
weight: 9
---
The deployment of the  SDaaS platform is based on Docker.
The SDaaS Enterprise Edition can also be installed on a standalone host (virtual or physical).
If you do not already have Docker on your computer, 
[it's the right time to install it](https://docs.docker.com/install/).


## Requirements
SDaaS is contained in a docker image
For SDaaS to operate effectively, certain prerequisites are necessary:
1. **Docker Orchestrator:** A capable Docker orchestrator such as Docker Compose or Kubernetes is essential. This orchestrator manages the deployment and execution of Docker images, ensuring efficient resource utilization and scalability.
2. **Accessible Graph Store:** The SDaaS platform requires access to at least one graph store. This store serves as the repository for the graph data and must be accessible by the SDaaS platform for storing and retrieving data as needed.

### Docker requirements
The SDaaS  requires:
- Docker version `~20.10`
- Docker Compose version `~2.17`


### Graph store requirements
SDaaS requires read/write access to at least one RDF Graph Store that requires:
- mandatory compliance to [HTTP/1.1 protocol](https://datatracker.ietf.org/doc/html/rfc9112) 
- mandatory compliance to [SPARQL 1.1 protocol](http://www.w3.org/TR/2013/REC-sparql11-protocol-20130321)
- mandatory compliance to [SPARQL 1.1 Query Language](http://www.w3.org/TR/2013/REC-sparql11-query-20130321)
- mandatory compliance to [SPARQL 1.1 Update](http://www.w3.org/TR/2013/REC-sparql11-update-20130321) specifications
- optional compliance to [SPARQL 1.1 Graph Store HTTP Protocol](http://www.w3.org/TR/2013/REC-sparql11-http-rdf-update-20130321)
- optional compliance to [SPARQL1.1 Service Description](http://www.w3.org/TR/2013/REC-sparql11-service-description-20130321)

The RDF Graph Store must provide an http(s) service endpoint compliant with the following minimal service description:
```
    @prefix sd: <http://www.w3.org/ns/sparql-service-description#>
    sd:supportedLanguage 
        sd:SPARQL11Query, 
        sd:SPARQL11Update ;
    sd:resultFormat 
        <http://www.w3.org/ns/formats/RDF_XML>, 
        <http://www.w3.org/ns/formats/Turtle>,
        <http://www.w3.org/ns/formats/N-Triples>,
        <http://www.w3.org/ns/formats/N-Quads>
    .
    sd:resultFormat
        <http://www.w3.org/ns/formats/RDF_XML>,
        <http://www.w3.org/ns/formats/Turtle>,
        <http://www.w3.org/ns/formats/N-Triples>
        <http://www.w3.org/ns/formats/SPARQL_Results_CSV>
    .
    sd:feature sd:UnionDefaultGraph.
```

The SDaaS license encompasses access to the `sdaas-rdfstore` graph store docker image, which is constructed upon a tailored iteration of the Blazegraph graph database. This customized version of Blazegraph is engineered to align with the specifications and demands set forth for a compliant graph store within the SDaaS framework.


## SDaaS docker image
The community edition image of SDaaS is available at https://hub.docker.com/r/linkeddatacenter/sdaas-ce

With your license you will receive a repository url and key that allows you to download the sdaas-ee docker image. Before using SDaaS it you need to login to the LinkedData.Center repository with using the received key:

```
docker login <sdaas repository URL you received>
```

## Customizing your SDaaS docker image
You can personalize your SDaaS instance writing your Dockerfile from this example:

```dockerfile
FROM linkeddatacenter/sdaas-(ce|ee):latest # you can substitute latest your preferred version id
## here your docker customization
```
then  build your docker image with the command:

```bash
docker build -t sdaas <here the path of your dockerfile>
```

Now you can store the create docker image in your private registry or keep it local. 

> **Do not save
your generated docker image in a public registry nor distribute it, because it contains information about your license and because this breaks
the license agreement.**


Enterprise Edition can use any RDF store or service compatible with SPARQL 1.1 specifications, e.g. LinkedData.Center SGaaS, Blazegraph, Stardog, AWS Neptune, virtuoso, etc. etc

The best security practices suggest to run SDaaS platform and the RDF store in a dedicated VPN, but this is not mandatory, you are free to adopt your preferred network topology.


## Configuration variables
These variables are defined by SDaaS docker, you can use them but should be considered **readonly** :

| Platform variable     | Default                         | Description                                                   |
|-----------------------|---------------------------------|---------------------------------------------------------------|
| AGENT_ID              | defined at script boot          | an unique id for the SDaaS running session                    |
| SDAAS_INSTALL_DIR     | /opt/sdaas                      | Root of the distribution of the SDaaS platform modules        |
| SDAAS_WORKSPACE       | /workspace                      | Default working directory                                     |
| SDAAS_ETC             | /etc/sdaas                      | Where internal configuration files are located                |
| SDAAS_REFERENCE_DOC   | https://linkeddata.center/sdaas | Base URL for command documentation (http/https)               | 


These variables have a global scope and can be changed runtime in docker instance or inside a script:

| Configuration variable | Default                         | Description                                                   |
|-----------------------|---------------------------------|---------------------------------------------------------------|
| SDAAS_APPLICATION_ID  | Community Edition               | Used in HTTP protocol agent signature                         |
| SD_LOG_PRIORITY       | 5                               | Sets the NOTICE default priority in logging                   |
| SD_ABORT_ON_FAIL      | false                           | Abort scripts on command failure                              |
| SD_DEFAULT_CONTEXT    | commands' context defaults      | it allows to specify some couple in the form <key>=<value> used modify the SDaaS commands' behavior |
| STORE                 | http://kb:8080/sdaas/sparql     | Default graph store SPARQL endpoint                           |
| STORE_TYPE            | w3c                             | Default store engine driver                                   |


{{% pageinfo color="info" %}}
Each SDaaS command has a local context that can be overridden by setting the SD_DEFAULT_CONTEXT global configuration variable or through command options and operands.

For instance, after setting `SD_DEFAULT_CONTEXT="sid=MYSTORE"` all subsequent calls to SDaaS commands will use the sid  `MYSTORE` instead of the default `STORE`  
{{% /pageinfo %}}

