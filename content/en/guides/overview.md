---
title: 🚀 Overview
linkTitle: Overview
date: 2023-11-23
description: An overview of the SDaaS platform
menu:
  main:
    weight: 1
weight: 1
---
SDaaS™ (Smart Data as a Service) is a software platform designed by _LinkedData.Center_ to build Enterprise _Knowledge Graphs_. 

The SDaaS platform empowers your applications, allowing them to integrate any [Linked Data](https://en.wikipedia.org/wiki/Linked_data) and leverage the full potential of the semantic web.  It is the reference implementation of _KEES_ (Knowledge Exchange Engine Specifications), facilitating the exchange and sharing of domain-specific knowledge.


```plantuml
node "User Application" as Application
cloud "Linked Data cloud" as RDFResource
    usecase "Data access" as query
package "Application Data Management System" {
    node "Smart Data service" as SDaaS #aliceblue;line:blue;line.dotted;text:blue
    database "Knowledge Graph" as KnowledgeGraph
    usecase "Data management" as maintain
}
Application -- query
KnowledgeGraph  -- query
SDaaS -- maintain
KnowledgeGraph -- maintain
KnowledgeGraph  <- SDaaS
RDFResource -> SDaaS

note top of SDaaS
Here is where the SDaaS platform is used
end note
```

The typical SDaaS user is a [DevOps](https://en.wikipedia.org/wiki/DevOps) professional who utilizes the commands provided by the platform to develop the service that learns, reasons, enriches and publish linked data within a _graph store_. The resulting knowledge graph is then queried by an application using SPARQL or REST APIs. SDaaS developers and system integrators can extend the platform by adding custom modules and creating new commands.


{{% pageinfo color="info" %}}
### What is a Knowledge Graph?
The knowledge graph is a pivotal component within any modern [Data Management Platform (DMP)](https://en.linkeddata.center/b/smart-data-platform) consisting in:
- a graph store containing first, second and third-party data;
- a formal knowledge organization that allows linking the data; 
- metadata to evaluate the data quality;
- axioms and rules computed by "reasoners" that are able to understand the meaning of the data;
- an interface to query the knowledge graph and to answers questions

### What is KEES?
*KEES  (Knowledge Exchange Engine Specifications and Services)* is an architectural design pattern that establishes specific requirements for Semantic Web Applications. Its purpose is to formally describe *domain knowledge* with the goal of making it tradeable and shareable. The full specifications are available on the [KEES homepage](http://LinkedData.Center/kees).
{{% /pageinfo %}}

## SDaaS™ features

SDaaS provides the following features:
- compliance with **W3C Semantic Web** standards 
- **compatibility** with various storage engines (e.g., Wikimedia BlazeGraph, AWS Neptune, etc., etc.) 
- **performance** optimization for the vendor implementations
- concurrent use of many knowledge instances for a virtually **unlimited capacity**
- **ingestion automation** of Linked Data according with [VoiD standard](https://www.w3.org/TR/void/)
- **data provenance** management automation according with [PROV ontology](https://www.w3.org/TR/2013/REC-prov-o-20130430/)
- extensible **deductive reasoning** through configurable OWL axioms materialization
- **abductive reasoning** through configurable rules
- full support of the **KEES specification**

SDaaS is available as:
- the *community edition platform* (ce), released as unsupported Open Source in 
  [GITHUB](https://github.com/linkeddatacenter/sdaas-ce)
  that provides a CLI interface and core tools for creating a knowledge base;
- the *enterprise edition platform* (ee), only available in the commercial version, that extends the community edition
  with additional tools and interfaces

SDaaS is a precious companion for:
- Anti-Money Laundry and Fraud detection (e.g. https://mospo.eu/)
- AI applications (e.g. https://EZClassifier.com and https://AdvisorLens)
- Identity management (eg. W3C Verifiable Credential schema)
- Financial data processing (e.g. https://budget.g0v.it/)
- Social Network
- Recommendation engines
- Scientific applications
- ... many other use case


{{% pageinfo color="info" %}}
<i class="fa-solid fa-hand-point-right fa-2xl"></i> have a look to the [Getting Started]({{< ref "getting started" >}}) guide to see SDaaS in action
{{% /pageinfo %}}


## Who wrote the code of SDaaS
_LinkedData.Center_ is a small technology company based in Europe, specializing in Neuro-Symbolic A.I. For more information and contacts, please visit the [LinkedData.Center website](https://LinkedData.Center/).