---
title: Advanced techniques 
description: Tip and tricks to unveil SDaaS potential
date: 2025-01-28
weight: 9
---

## KEES fast boot
The command [sd kees boot]({{< ref "sd_kees_boot" >}}) (only Enterprise Edition) uses an optimized algorithm to cleanup the knowledge graph adding all metadata required by KEES specifications.



## Managing the Knowledge Base Status (EE)
You can signal the publication status of a specific knowledge base using KEES status poperties. 

For setting, getting, and checking the status of a specific window in the knowledge graph, use:

```bash
# prints the date of the last status changes:
sd kees date published

# test a status
sd kees is published || echo "Knowledge Graph is not Published"
sd kees is stable || echo "Knowledge Graph is not in a stable status"
```


### Connecting to multiple RDF Graph Stores
You can direct the SDaaS platform to connect to multiple RDF store instances, using standard or optimized drivers: 

```bash
AWS="http://mystore.example.com/sparql"
AWS_TYPE="neptune"
WIKIDATA="https://query.wikidata.org/sparql"
WIKIDATA_TYPE="w3c"
```
 
This allow to import or reasoning using specialized SPARQL end point
For instance, the above example imports all cat from wikidata into the default graph store and then list the first five cat names:

```bash
cat <<EOF | sd sparql rule -s WIKIDATA | sd sparql graph
DESCRIBE ?item WHERE { 
        ?item wdt:P31 wd:Q146 
} 
EOF

cat <<EOF | sd sparql list
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
SELECT DISTINCT ?cat WHERE {
        ?item wdt:P31 wd:Q146; rdfs:label ?cat
        FILTER( LANG(?cat)= "en")
} ORDER BY ?cat LIMIT 5 
EOF
```

## Scripting
You have the ability to create a bash script containing various commands. 

Refer to the [Application building guide]({{< ref "application building" >}}) for more info about SDaaS scripting

