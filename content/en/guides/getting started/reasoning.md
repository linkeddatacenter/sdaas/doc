---
title: Reasoning 
description: How to reasoning about facts in the knowledge graph
date: 2025-01-28
weight: 8
---

## Reasoning about facts
To materialize inference in the knowledge graph you have some options available:
- using [sd sparql update]({{< ref sd_sparql_update >}}) commands using SPARQL Update or SPARQL Query constructors 
- using the [sd sparql rule]({{< ref sd_sparql_rule >}}) with constructors
- using the [sd learn rule]({{< ref sd_learn_rule >}}) (EE) with constructors

You can use SPARQL update or a combination of SPARQL query as in the following examples:

```bash
# insert data evaluates the expression at SPARQL server side.
cat <<EOF | sd sparql update
INSERT {...} 
WHERE {...}
EOF
        

#  pipe two commands
cat <<EOF | sd sparql rule | sd sparql graph
CONSTRUCT ...
EOF
        
# pipe four commands adding metadata
cat <<EOF | sd sparql rule | sd kees metadata -D "trust=0.9" urn:graph:mycostructor | sd sparql graph urn:graph:mycostructor
CONSTRUCT ...
EOF

# same of above shortcut:
cat <<EOF | sd learn rule -D "trust=0.9" urn:graph:mycostructor
CONSTRUCT ...
EOF
```



## Using plans (EE)
The [plan module](/module/plan) provides some specialized commands to run a SDaaS scripts

You can think a plan similar to a stored procedure in a SQL database. 

For example, assume that STORE contains the following three plans:

```turtle
prefix sdaas: <http://linkeddata.center/sdaas/reference/v4#> .

<urn:myapp:cities> a sdaas:Plan;   sdaas:script """
        sd learn resource -D "graph=urn:dataset:dbpedia" http://dbpedia.org/resource/Milano
        sd learn resource -D "graph=urn:dataset:dbpedia" http://dbpedia.org/resource/Lecco
""" .

<urn:reasoning:recursive> a sdaas:Plan ; sdaas:script """
        sd learn rule 'CONSTRUCT ...'
        sd learn rule http://example.org/rules/rule1.rq 
""" .
<urn:test:acceptance> a sdaas:Plan ; sdaas:script """
        sd_curl_sparql http://example.org/tests/test1.rq | sd sparql test
        sd_curl_sparql http://example.org/tests/test2.rq | sd sparql test
""" .
```

Then this commands use plans to automate some common activities:

```bash
sd plan run -D "activity_type=Ingestion" urn:myapp:cities
sd plan loop -D "activity_type=Reasoning trust=0.75" urn:reasoning:recursive
sd -A plan run -D "activity_type=Publishing" urn:test:acceptance
```

{{% pageinfo color="info" %}}
<i class="fa-solid fa-eye fa-2xl"></i>

the `sd plan loop` command executes a plan until there are no more changes in the knowledge base. It is useful to implement incremental or recursive reasoning rules.
{{% /pageinfo %}}


## Managing the Knowledge Base Status (EE)
You can signal the publication status of a specific knowledge base using KEES status poperties. 

For setting, getting, and checking the status of a specific window in the knowledge graph, use:

```bash
# prints the date of the last status changes:
sd kees date published

# test a status
sd kees is published || echo "Knowledge Graph is not Published"
sd kees is stable || echo "Knowledge Graph is not in a stable status"
```


### Connecting to multiple RDF Graph Stores
You can direct the SDaaS platform to connect to multiple RDF store instances, using standard or optimized drivers: 

```bash
AWS="http://mystore.example.com/sparql"
AWS_TYPE="neptune"
WIKIDATA="https://query.wikidata.org/sparql"
WIKIDATA_TYPE="w3c"
```
 
This allow to import or reasoning using specialized SPARQL end point
For instance, the above example imports all cat from wikidata into the default graph store and then list the first five cat names:

```bash
cat <<EOF | sd sparql rule -s WIKIDATA | sd sparql graph
DESCRIBE ?item WHERE { 
        ?item wdt:P31 wd:Q146 
} 
EOF

cat <<EOF | sd sparql list
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
SELECT DISTINCT ?cat WHERE {
        ?item wdt:P31 wd:Q146; rdfs:label ?cat
        FILTER( LANG(?cat)= "en")
} ORDER BY ?cat LIMIT 5 
EOF
```

## Scripting
You have the ability to create a bash script containing various commands. 

Refer to the [Application building guide]({{< ref "application building" >}}) for more info about SDaaS scripting


## Quitting the platform
When you type `exit` you can safely destroy the sdaas container but the created data will persist in the external store.

Free allocated docker resources by typing:

```
docker rm -f kb
docker network rm myvpn
```