---
title: "Driver building"
description: "how write a custom graph store driver"
weight: 6
---

## Conformance Notes
This chapter defines some conventions to extend the SDaaS platform.

Conformance requirements are expressed with a combination of descriptive assertions and [RFC 2119](https://tools.ietf.org/html/rfc2119) terminology. The key words **MUST**, **MUST NOT**, **REQUIRED**, **SHALL**, **SHALL NOT**, **SHOULD**, **SHOULD NOT**, **RECOMMENDED**, **MAY**, and **OPTIONAL** in the normative parts of this document are to be interpreted as described in RFC 2119.  However, for readability, these words may not always appear in uppercase letters in this specification.


## Driver  Requirements
- a driver MUST be a valid a [SDaaS module]({{ < ref "module building" >}})
- it MUST implements the driver interface with SDaaS functions that conforms to the  following naming conventions:
`sd_<driver name>_<driver method>`; for example if you want to implement a special the driver for the `stardog` graph store you MUST implement the `sd_stardog_query` function
- all driver function MUST be positional, with no defaults. The validity check of the parameters is responsability of the caller (ie. the driver module)
- Following  methods MUST be implemented:

| method name( parameters ) | description|
|---------------------------|------------|
| erase(sid) | erase the graph store |
| load(sid,  graph, accrual_method) | loads a stream of nTripes into a named graph in a graph store according the accrual policy "
| query(sid, mime_type)  | execute in a graph store a SPARQL query read from std input, requesting in output one of the supported mime types
| size(sid) | return the number of triples in a store |
| update(sid) | execute in a graph store a SPARQL update request read from std input

All method parameters are strings that MUST matches following regular expressions:
- *sid* MUST match the `^[a-zA-Z]+$` regular expression and MUST point to an http(s) URL. It is assumed that sid is the name of a _SID variable_
- *graph* MUST be a valid URI
- *mime_type* MUST be a valid mime type
- *accrualMethod* MUST match `PUT` or `POST` 


## Implementing a new graph store driver
A driver is the implementation is described by the following UML class diagram:


```plantuml
package "SDaaS Community Edition" {
    interface DriverInterface
    DriverInterface : + erase(sid)
    DriverInterface : + load(sid, graph, accrual_method)
    DriverInterface : + query(sid, mime_type)
    DriverInterface : + size(sid)
    DriverInterface : + update(sid)
    DriverInterface : + validate(sid)

    class w3c
    class testdriver
}
package "SDaaS Enterprise Edition" {
    class blazegraph
    class gsp
    class neptune
}

testdriver ..|> DriverInterface
w3c ..|> DriverInterface 
blazegraph --|> w3c
gsp --|> w3c
neptune --|> w3c
```

There is a reference implementation of the driver interface known as the `w3c` driver compliant with [W3C SPARQL 1.1 protocol](https://www.w3.org/TR/sparql11-protocol/) specification, along with the `testdriver` stub driver implementation to be used in unit tests. The SDaaS Enterprise Edition offers additional drivers that are specialized versions of the `w3c` driver, optimized for specific graph store technologies:
- **gsp driver**: a `w3c` driver extension that uses the [SPARQL 1.1 Graph Store HTTP Protocol](https://www.w3.org/TR/sparql11-http-rdf-update/). It defines the *configuration variable* `<sid>_GSP_ENDPOINT` that contains the http address of the service providing a Graph Store Protocol interface.
- **blazegraph driver**: a optimized implementation for Blazegraph graph store.
- **neptune driver**: a optimized implementation for AWS Neptune service.

{{% pageinfo color="warning" %}}
<i class="fa-solid fa-eye fa-2xl"></i> In commands, do not call the driver method implementation function directly. Instead, call the corresponding *abstract driver module* functions.
{{% /pageinfo %}}


