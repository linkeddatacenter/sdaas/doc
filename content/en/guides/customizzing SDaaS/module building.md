---
title: "Module building"
description: how to extend the platform creating new modules
weight: 5
---


## Conformance Notes
This chapter defines some conventions to extend the SDaaS platform.

Conformance requirements are expressed with a combination of descriptive assertions and [RFC 2119](https://tools.ietf.org/html/rfc2119) terminology. The key words **MUST**, **MUST NOT**, **REQUIRED**, **SHALL**, **SHALL NOT**, **SHOULD**, **SHOULD NOT**, **RECOMMENDED**, **MAY**, and **OPTIONAL** in the normative parts of this document are to be interpreted as described in RFC 2119.  However, for readability, these words may not always appear in uppercase letters in this specification.

## Module Requirements
An SDaaS module is a bash script file fragment that complies with the following restrictions:

- It **MUST** be placed in the `SDAAS_INSTALL_DIR` or in the modules directory in the `$HOME/modules` directory. Modules defined in the `$HOME/modules` directory take precedence over the ones defined in the `SDAAS_INSTALL_DIR` directory.
- Its name **MUST** match the regular expression `^[a-z][a-z0-9-]+$`.
- The first line of the module **MUST** be `if [[ ! -z ${__module_MODULENAME+x} ]]; then return ; else __module_MODULENAME=1 ; fi` where MODULENAME is the name of the module.
- all commands defined in the module SHOULD match sd_MODULENAME_FUNCTIONAME here the MODULENAME is the same name of the module that contains the command and FUNCTIONNAME is an unique name inside the module. To rewrite existing  commands it is allowed but discouraged.
- all commands MUST follow the syntax conventions described below.


A module CAN contain:
1) **Constants** (read only variables): constant SHOULD be prefixed by SDAAS_ and MUST be an unique name in the platform. Overrriding default 
2) **Configuration variables**: constant SHOULD be prefixed by SD_ and MUST be an unique name in the platform. 
3) **Module commands** definition. 
4) **Module initialization**: a set of bash commands that always runs on module loading

For example, see the modules implementation in [SDaaS community edition](https://github.com/linkeddatacenter/sdaas-ce)


## Command requirements
Commands MUST support the Utility Syntax Guidelines described in the Base Definitions volume of POSIX.1‐2017, Section 12.2, from section  3 to 10, inclusive.

All commands that accepts options and/or operands SHOULD accept also the option `-D`. Such option is used for define local variables in the form of key-value that provides a context for the command. 

Options SHOULD conform to the following naming conventions:

`-A`
: abort if the command returns is > 0 . 


-a, -D "accrual_method=*ACCRUAL METHOD*"
: accrual method, the method by which items are added to a collection. PUT and POST method SHOULD be recognized


-f *FILE NAME*, -D "input_file=*FILE NAME*"
:  to be use to refer a local file object with relative or absolute path


-i **INPUT FORMAT**, -D "input_format=*INPUT FORMAT*"
: input format specification, these values SHOULD be recognized (from libraptor):

| Format      | Description                                       |
|-------------|---------------------------------------------------|
| rdfxml      | RDF/XML (default)                                 |
| ntriples    | N-Triples                                         |
| turtle      | Turtle Terse RDF Triple Language                  |
| trig        | TriG - Turtle with Named Graphs                    |
| guess       | Pick the parser to use using content type and URI  |
| rdfa        | RDF/A via librdfa                                 |
| json        | RDF/JSON (either Triples or Resource-Centric)     |
| nquads      | N-Quads                                           |


-h
: prints an help description


-o *OUTPUT FORMAT*, -D "output_format=*OUTPUT FORMAT*"
: output format specification This values SHOULD be recognized:

- csv
- csv-h
- csv-1
- csv-f1
- boolean
- tsl
- json
- ntriples
- xml
- turtle
- rdfxml
- test


-p *PRIORITY*
: to be use to reference a priority 

| level | mnemonic | explanation |
| -------------- | ----------------- |-------------|
| 2              | CRITICAL          | Should be corrected immediately, but indicates failure in a primary system - fix CRITICAL problems before ALERT - an example is loss of primary ISP connection. |
| 3              | ERROR             | Non-urgent failures - these should be relayed to developers or admins; each item must be resolved within a given time. |
| 4              | WARNING           | Warning messages - not an error, but indicated that an error will occur if action is not taken, e.g. file system 85% full - each item must be resolved within a given time. |
| 5              | NOTICE            | Events that are unusual but not error conditions - might be summarized in an email to developers or admins to spot potential problems - no immediate action required. |
| 6              | INFORMATIONAL     | Normal operational messages - may be harvested for reporting, measuring throughput, etc. - no action required. |
| 7              | DEBUG             | Info is useful to developers for debugging the app, not useful during operations. |


-s *SID* , "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

-S *SIZE*
: to be use to reference a size


### Evaluation of the local command context
The process of evaluating the local context for a command is as follows:

1) First, the local context hardcoded in the command implementation is evaluated.
2) The hardcoded local context can be overridden by the global configuration variable **SD_DEFAULT_CONTEXT**.
3) The resulting context can be overridden by specific **command options** (e.g., -s *SID*). Command options are evaluated left to right
4) The resulting context can be overridden by specific **command operands**.

For example, all these calls will have the same result:
- `sd_sparql_graph`: the hardcoded local context ingests data into a named graph inside the graph store connected to the sid _STORE_, using a generated UUID URI as the name of the named graph.
- `sd_sparql_graph -s STORE $(sd_uuid)`: hardcoded local context overridden by specific options and operand.
- `sd_sparql_graph -D "sid=STORE`: hardcoded local context overridden by the -D option.
- `sd_sparql_graph -D "sid=STORE" -D "graph=$(sd_uuid)"`: the same as above but using multiple -D options (always evaluated left to right).
- `SD_DEFAULT_CONTEXT="sid=STORE"; sd_sparql_graph $(sd_uuid)`: hardcoded local context overridden by SD_DEFAULT_CONTEXT and operand.
- `SD_DEFAULT_CONTEXT="sid=XXX"; sd_sparql_graph -s YYY -D "sid=STORE graph=ZZZZ" $(sd_uuid)`: a silly command call that demonstrates the overriding precedence in local context evaluation.

