---
title: About SDaaS
linkTitle: About
menu: {main: {weight: 90}}
---

{{% blocks/cover title="Contact Us" image_anchor="bottom" height="auto" %}}


{{% /blocks/cover %}}

{{% blocks/lead %}}

SDaaS™ is a product provided by [LinkedData.Center](https://linkeddata.center)

For a special quote, POC and more info, please contact us at info@LinkedData.Center or send a telegram message to +393356382949

{{% /blocks/lead %}}

