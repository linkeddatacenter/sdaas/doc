---
title: sd_view_module
description: display all functions exported by a module
module: view
component: Command
distribution:
    - Community Edition
    - Enterprise Edition
---
Displays all available commands exported by a module with their cache status


# Synopsys
sd view commands *MODULE* (`core` by default)


MODULE
: a module name 

Examples:
```bash
sd view module core
```