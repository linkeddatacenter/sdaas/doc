---
title: sd_include
description: include a module
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_include** [-f] *MODULE*

### Description
include *MODULE* only if it is not already loaded, used by [sd](../sd) command.

This command tries to load *MODULE* form $HOME/modules/ directory and, if file not exist, tries to load it from $SDAAS_INSTALL_DIR . 

-f
: force the module cache to stale and re- load the module

### Exit status
Exits 0 on success, and >0 if an error occurs.

### Availability
Since SDaaS 4

### Examples
`sd_include core || sd_abort`
: includes all commands exposed by the module `core` just the first time. Abort scripts on fail


`sd_include -f core`
: reload all commands exposed by the module `core` ignoring its cache status
