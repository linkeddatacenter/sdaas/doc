---
title: sd_kees_metadata
description: print kees metadata
module: kees
component: Ingestion
distribution:
    - Enterprise Edition
---

### Synopsis

**sd_kees_matadata** [-D *METADATA* ] *GRAPH NAME*


### Description

print the metadata about a named graph with name  *GRAPH NAME* serialized in in nTriples. This command is mainly intended for internal use only.

*GRAPH NAME*, -D "graph=*GRAPH NAME*"
: an URI for the graph name

*METADATA*
: following metadata are supported:


| Variable Name         | Default                         | Description                                                   |
|-----------------------|---------------------------------|---------------------------------------------------------------|
| trust                 | 1                               | Trust level as a xds:decimal ranging 0..1                     |
| activity_type         | Learning                        | a KEES activity type (without namespace) OR xsd:anyURI        |
| graph                 | *uuid*                          | XSD:anyURI Named graph name                                   |
| graph_type            | FirstPartyData                  | a KEES named_graph type (without namespace) OR xsd:anyURI     |
| source                |                                 | XSD:anyURI The data source                                    |
| activity_id           | *uuid*                          | XSD:anyURI Identifier for a prov:Activity                     |
| started_at            | <now>                           | activity start date                                           | 



### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
Exits 0 on success, and >0 if an error occurs.

### Availability
Since SDaaS 4