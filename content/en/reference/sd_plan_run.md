---
title: ⚗️sd_plan_run
description:  execute a sdaas
component: Compound Command
distribution:
    - Enterprise Edition
---

### Synopsis

**sd_plan_run**  [-s *SID*] [-D *METADATA*]  *PLAN_URI*


### Description
This command executed a script exposed by *PLAN_URI* in the   `sdaas:script` property 

*PLAN_URI*
: the uri of a sdaas:Plan subject

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)


### Exit status
Exits with 0 on success, and > 0 if error occurs. 

### Availability
Since SDaaS 4

### Example
sd plan run urn:my:plan

