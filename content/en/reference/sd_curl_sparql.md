---
title: sd_curl_sparql
description: a wrapper for curl with content management for SPARQL command files
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_curl_sparql**  *ARGUMENTS*

### Description
It is an alias for `sd_curl -s -f -H "Accept: application/sparql-query, application/sparql-update;q=0.9, */*;q=0.5" "$@"`

### Exit status
Exits with 0 on success, and  not 0 if a curl or http error occurs.

### Availability
Since SDaaS 4
