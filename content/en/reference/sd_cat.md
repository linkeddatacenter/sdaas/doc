---
title: sd_cat
description: a wrapper for cat
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_cat**  *cat arguments*

### Description
its an alias for: `cat "$@" 2> /dev/null`

### Exit status
Exits with 0 on success

### Availability
Since SDaaS 4
