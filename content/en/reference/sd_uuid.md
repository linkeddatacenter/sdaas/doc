---
title: sd_uuid
description: generate an uuid
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_uuid** [*NAMESPACE*]

### Description

write an uuid URI minted from *NAMESPACE* (urn:uuid: by default)

### Availability
Since SDaaS 4

### Examples

`sd_uuid`
: write the string "un:uuid:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" were _x_ represents a random hexadecimal digit

`sd_uuid "" `
: just write the uuid string without any prefix
