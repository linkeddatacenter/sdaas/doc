---
title: sd_kees_date
module: kees
component: Query
distribution: enterprise edition
---

### Synopsis

**sd_kees_date**  [-D "sid=*SID* ] [-s *SID*] *STATUS*

### Description
Prints the date of one *STATUS*, prints the empty string if the status is not available.

*STATUS*
: is one one of :

- created
- ingested
- reasoned
- enriched
- published
- versioned

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
Exits 0 on success, and > 0 if an error occurs.

### Availability
Since SDaaS 4 - Enterprise Edition

### Examples

`sd kees date published`
: returns the last date of the knowledge base publishing (empty string if never published)