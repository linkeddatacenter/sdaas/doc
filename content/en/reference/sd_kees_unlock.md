---
title: sd_kees_unlock
module: kees
component: Ingestion
distribution: enterprise edition
---

### Synopsis

**sd_kees_unlock** [-D "sid=*SID* ] [-s *SID*] 

### Description

Locks the knowledge graph with the *ACTIVITY_URI*.

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
Exits 0 on success, and > 0 if an error occurs.

### Availability
Since SDaaS 4 - Enterprise Edition

### Examples

`sd kees unlock`
: unlock the knowledge graph
