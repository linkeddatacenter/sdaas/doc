---
title: sd_sparql_graph
description: load a set of nTriples into a named graph within a graph store
module: sparql
component: Ingestion
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_sparql_graph** [-s *SID*]  [ -a *ACCRUAL METHOD* ] [*GRAPH_URI*]

### Description
Load a stream  of nTriples into the GRAPH_URI. No action is performed if an empty data stream is detected.

*GRAPH_URI*
: it is the URI for a named graph in the store that will contain the resource. If *GRAPH_URI* is unspecified, it uses `$(sd_uuid)` as the graph name: note that this implies that a new named graph is generated every call even if  `POST` is used as *ACCRUAL METHOD*.


-a *ACCRUAL METHOD*
: When  `PUT`, the graph will be cleared before loading the data, when `POST` (default) the data is appended the graph, creating it if it doesn't exist.

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
Exits 0 on success.

### Availability
Since SDaaS 4

### Examples

`sd_curl https://schema.org/version/latest/schemaorg-current-http.nt | sd sparql graph "urn:graph:1"`
: download and stores the content of the resource into the named graph `urn:graph:1`


`sd_curl https://schema.org/version/latest/schemaorg-current-http.nt | sd sparql graph`
: same of above but guessing and translating the RDF content of the resource storeing its if to a named graph with random uuid URI
