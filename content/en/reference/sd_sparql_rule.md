---
title: sd_sparql_rule
description: a sparql query alias
module: core
component: Facts Provision
distribution:
    - Community Edition
    - Enterprise Edition
---

This is an alias for [sd sparql query]({{< ref "sd_sparql_query" >}}) -o ntriples


### Availability
Since SDaaS 4
