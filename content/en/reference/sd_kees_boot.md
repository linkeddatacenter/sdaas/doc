---
title: sd_kees_boot
module: kees
component: Ingestion
distribution: enterprise edition
---

### Synopsis

**sd_kees_boot** [-D "sid=*SID* ] [-s *SID*]

### Description
The command erases all data in a graph store using a driver-optimized routine. Additionally, this command adds metadata to the default graph according with the KEES specifications, as shown in this snippet:

```turtle
[] a kees:KnowledgeGraph ;
    sd:endpoint <$STORE> ;
    sd:feature kees:Status, kees:Locking ;
    dct:created "2023-12-10T01:00:01Z"^^xsd:dateTime .
```
sd_kees_boot preserves locks (if any)


-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable


### Exit status
Exits 0 on success, and > 0 if an error occurs.

### Availability
Since SDaaS 4 - Enterprise Edition

### Examples

`sd_kees_boot `
: execute the command `DROP ALL` on the Graph Store named `STORE` (equivalent to `sd_sparql_update"DROP ALL"`)

`sd_kees_boot -s STORE2 `
: same of above but using the Graph Store identified by the sid `STORE2`
