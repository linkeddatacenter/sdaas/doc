---
title: sd_sparql_update
description: update a graph store with SPARQL
module: sparql
component: Ingestion
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_sparql_update** [-s *SID*] [-D *LOCAL CONTEXT*]

### Description
execute the SPARQL update string passed stdin

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
0 on success

### Availability
Since SDaaS 4

### Examples

These equivalent commands execute `DROP ALL` on the Graph Store named `STORE`:

```bash
echo "DROP ALL" | sd sparql update
echo "DROP ALL" | sd sparql update -s STORE
echo "DROP ALL" | sd sparql update -D "sid=STORE"
# same of above but aborting on failure
DROP ALL" | sd -A sparql update
```
