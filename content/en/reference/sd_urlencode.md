---
title: sd_urlencode
description: url encode a string
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

url encode a string substituting unsafe chars

### Synopsis

**sd_urlencode** *STRING*

### Description

url encodes *STRING*

### Availability
Since SDaaS 4

### Examples

`sd_urlencode "http://w3.org/?test#name>"`
: write the string http%3A%2F%2Fw3.org%2F%3Ftest%23name%3E"
