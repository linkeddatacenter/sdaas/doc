---
title: sd_kees_describe
module: kees
component: Facts Provision
distribution: enterprise edition
---

### Synopsis

**sd_kees_describe** [-D "sid=*SID* ] [-s *SID*] 

### Description
Describe the knowledge base as a set of nTriples


**ACTIVITY_URI**
An URI of a prov:Activity. In missing a uuid is generated.


-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
Exits 0 on success, and > 0 if an error occurs.

### Availability
Since SDaaS 4 - Enterprise Edition
