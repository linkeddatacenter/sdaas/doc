---
title: sd_return_first_non_zero
description: analyze the return of a piped command
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_return_first_non_zero**  *PIPESTATUS*

Used in combiantion with PIPESTATUS bash variable

### Exit status

Returns firs non zero value in the arguments


### Availability
Since SDaaS 4


### Examples

```
cmd1 | cmd3 | cmd3
sd_return_first_non_zero "${PIPESTATUS[@]}"
```
