---
title: sd_curl
description: a wrapper for curl
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_curl**  *CURL ARGUMENTS*

### Description
its an alias for: `curl -L -A "$SDAAS_SIGNATURE" --retry 3 --retry-delay 3 --retry-max-time 30 --retry-connrefused --compressed`

### Exit status
Exits with 0 on success, and  not 0 if a curl or http error occurs.

### Availability
Since SDaaS 4
