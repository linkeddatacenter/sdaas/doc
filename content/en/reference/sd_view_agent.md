---
title: sd_view_agent
description: describe SDaaS agent properties
module: view
component: Facts Provision
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_view_agent** [-o *FORMAT* ]


### Description

print the SDaaS agent in the  RDF *FORMAT*


-o *OUT_FORMAT*
: one of these formats: ntriples(default), turtle, rdfxml



### Exit status
Exits 0 on success, and >0 if an error occurs.

### Availability
Since SDaaS 4