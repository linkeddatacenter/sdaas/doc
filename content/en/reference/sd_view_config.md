---
title: sd_view_config
description: display all declared configuration variables
module: view
component: Command
distribution:
    - Community Edition
    - Enterprise Edition
---
The sd view config displays all defined configuration and driver variables


# Synopsys
sd view config  
