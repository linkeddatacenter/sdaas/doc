---
title: sd_view_ontology
description: print the SDaaS language Profile
module: view
component: Facts Provision
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_view_ontology** [-o *FORMAT* ]


### Description

print the SDaaS language Profile including KESS support in the  RDF *FORMAT*


-o *OUT_FORMAT*
: one of these formats: ntriples(default), turtle, rdfxml



{{% pageinfo color="info" %}}
<i class="fa-solid fa-hand-point-right fa-2xl"></i> for a definition of all SDaaS Language Profile see the  [SDaaS Ontology]({{< ref v4 >}}) guide to see SDaaS in action
{{% /pageinfo %}}


### Exit status
Exits 0 on success, and >0 if an error occurs.

### Availability
Since SDaaS 4