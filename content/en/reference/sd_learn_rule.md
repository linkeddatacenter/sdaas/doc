---
title: sd_learn_rule
module: learn
component: Learning
distribution:
    - Enterprise Edition
---

### Synopsis

**sd_learn_rule** [-s *SID*] [-a PUT|POST] [-D *METADATA*] 

### Description

This command is essentially the same as the combination of the following piped commands:

	sd sparql rule | sd kees metadata | sd sparql graph 

-s *SID*
: connect to Graph Store named *SID* ( `STORE` by default)

-a PUT|POST
: the graph accural policy. can be PUT  or POST. With PUT data substitute existing graph, with POST data is appended to the graph

-D *METADATA*
: the following local variables are supported:

| Variable Name         | Default                         | Description                                                   |
|-----------------------|---------------------------------|---------------------------------------------------------------|
| sid                   | STORE                           | same as -s                                                    |
| trust                 | 1                               | Trust level as a xds:decimal ranging 0..1                     |
| activity              | Reasoning                       | a KEES activity type (without namespace) OR xsd:anyURI        |
| graph                 | urn:sdaas:abox                  | XSD:anyURI Named graph name                                   |
| activity_id           | *uuid*                          | XSD:anyURI Identifier for a prov:Activity                     |
| started_at            | <now>                           | activity start date                                           | 
| accrual_policy        | POST                            | same as -a


### Exit status
Exits with 0 on success, and > 0 if error occurs. 

### Availability
Since SDaaS 4

