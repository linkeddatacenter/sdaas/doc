---
title: sd_kees_lock
module: kees
component: Ingestion
distribution: enterprise edition
---

### Synopsis

**sd_kees_lock** [-D "sid=*SID* ] [-s *SID*] [-a AGENT_URI ] [*ACTIVITY_URI*]

### Description
Locks the knowledge graph with the *ACTIVITY_URI*. The lock is removed on EXIT signal.

If a knowledge graph is locked, you can manually unlock it with the SPARQL update:
```sparql
PREFIX kees: <http://linkeddata.center/kees/v1#>
DELETE  { ?x kees:isLockedBy ?y }
WHERE  { ?x kees:isLockedBy ?y }
```


**ACTIVITY_URI**
An URI of a prov:Activity. If missing, the value of the ACTIVITY_URI is used.

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)


-a AGENT_URI
: uses AGENT_URI as the activity URI used to mark the lock. By default is AGENT_ID

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
Exits 0 on success, and > 0 if an error occurs.

### Availability
Since SDaaS 4 - Enterprise Edition

### Examples

`sd kees lock`
: set the knowledge graph locked
