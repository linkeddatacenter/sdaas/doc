---
title: ⚗️sd_plan_loop
description:  execute a sdaas plan in loop till no changes in graph stores
component: Compound Command
distribution:
    - Enterprise Edition
---

Equivalent to:

```bash
previous_size=-1; actual_size=$(sd driver size STORE)
while [[ $previous_size -ne $actual_size ]]; do
    previous_size=$actual_size
    sd plan run -D "activity_type=Abduction trust=0.9" urn:myapp:reasoning_plan
    actual_size=$(sd driver size STORE)
done
```
### Synopsis

**sd_plan_loop**  [-s *SID*] [-D *METADATA*]  *PLAN_URI*


### Description
This command executed execute a plan until nothing changes. It is equivalent to the following pseudocode:

```bash
previous_size=-1; actual_size=$(sd driver size *SID*)
while [[ $previous_size -ne $actual_size ]]; do
    previous_size=$actual_size
    sd plan loop run  *PLAN_URI*
    actual_size=$(sd driver size *SID*)
done
```

*PLAN_URI*
: the uri of a sdaas:Plan subject

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)


### Exit status
Exits with 0 on success, and > 0 if error occurs. 

### Availability
Since SDaaS 4

### Example
sd plan run urn:my:plan

