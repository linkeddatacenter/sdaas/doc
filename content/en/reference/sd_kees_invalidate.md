---
title: sd_kees_invalidate
module: kees
component: Ingestion
distribution: enterprise edition
---

### Synopsis

**sd_kees_invalidate** [-D "sid=*SID* ] [-s *SID*]  [*ACTIVITY_URI*]

### Description

Invalidate the knowledge graph with the *ACTIVITY_URI*.


**ACTIVITY_URI**
An URI of a prov:Activity. In missing a uuid is generated.


-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
Exits 0 on success, and > 0 if an error occurs.

### Availability
Since SDaaS 4 - Enterprise Edition

### Examples

`sd kees sd_kees_invalidate`
: set the knowledge base `prov:wasInvalidatedBy` to to a uuid of thoe prov:Activity
