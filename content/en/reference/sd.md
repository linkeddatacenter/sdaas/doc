---
title: sd
description: call a SDaaS command
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd** [-h] [-A]  *MODULE*  *FUNCTION* [*FUNCTION ARGS*]

### Description
Execute a SDaaS platform *FUNCTION* by automatically including *MODULE* and running the command `sd_<MODULE>_<FUNCTION>` followed by *FUNCTION ARGS*

-A
: abort if the command returns is > 0 .

Note that negative values does not trigger an exit because negative return status are considered by SDaaS platform as boolean false result.

-h
: prints an help description about the command


### Exit status
If not -A specified return the command status

### Availability
Since SDaaS 4

### Examples

`sd sparql query "SELECT * { ?s ?p ?o } LIMIT 1"`
: Prints an RDF triple in the `STORE` instance.

`sd -A sparql query "SELECT * { ?s ?p ?o } LIMIT 1"`
: Similar to the previous command, aborts on any _sparql query_ failure.

`sd -h sparql query`
: prints the manual of sd_sparql_query command
