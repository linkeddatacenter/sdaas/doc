---
title: sd_store_erase
description: erase a graph store 
module: store
component: Ingestion
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_store_erase** [-s *SID*] [-D *LOCAL CONTEXT*]

### Description
remove all data in the knowledge graph it is functionally equivalent to `echo "DROP ALL" | sd sparql update`

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
0 on success

### Availability
Since SDaaS 4


