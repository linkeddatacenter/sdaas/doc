---
title: sd_learn_file
description: "loads an RDF file with its provenance metadata"
module: learn
component: Learning
distribution:
    - Enterprise Edition
---

### Synopsis

**sd_learn_file** [-s *SID*] [-a PUT|POST] [-D *METADATA*] *FILE*

### Description
Loads a file into the graph store. This command is essentially the same as the combination of the following piped commands:

	sd_rapper | sd kees meta | sd sparql graph 

*FILE*
: the path of a local file (both relative or absolute). COuld be omitted if -D "input_file=*FILE*" is present.

-s *SID*
: connect to Graph Store named *SID* ( `STORE` by default)

-a PUT|POST
: the graph accural policy. can be PUT  or POST. With PUT data substitute existing graph, with POST data is appended to the graph

-D *METADATA*
: the following local variables are supported:

| Variable Name         | Default                         | Description                                                   |
|-----------------------|---------------------------------|---------------------------------------------------------------|
| sid                   | STORE                           | same as -s                                                    |
| trust                 | 1                               | Trust level as a xds:decimal ranging 0..1                     |
| activity_type         | Learning                        | a KEES activity type (without namespace) OR xsd:anyURI        |
| graph                 | same of source                  | XSD:anyURI Named graph name                                   |
| graph_type            | FirstPartyData                  | a KEES named_graph type (without namespace) OR xsd:anyURI     |
| source                | file://localhost/<absolute *FILE* path> | XSD:anyURI in file: schema                            |
| activity_id           | *uuid*                          | XSD:anyURI Identifier for a prov:Activity                     |
| started_at            | <now>                           | activity start date                                           | 
| accrual_policy        | POST                            | same as -a


### Exit status
Exits with 0 on success, and > 0 if error occurs. 

### Availability
Since SDaaS 4

### Example

sd learn file -D "graph=urn:app:config" /etc/config.ttl