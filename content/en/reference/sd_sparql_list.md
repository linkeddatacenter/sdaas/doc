---
title: sd_sparql_list
description: a sparql query alias
module: core
component: Query
distribution:
    - Community Edition
    - Enterprise Edition
---

This is an alias for [sd sparql query]({{< ref "sd_sparql_query" >}}) -o "csv-h" 


### Availability
Since SDaaS 4
