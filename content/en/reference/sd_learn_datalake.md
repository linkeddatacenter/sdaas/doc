---
title: ⚗️ sd_learn_datalake
module: learn
component: Learning
distribution:
    - Enterprise Edition
---

### Synopsis

**sd_learn_datalake**  -D [*METADATA*] [-D "graph=*GRAPHNAME*"]  *DATA_ROOT*

### Description
Discovers and loads datasets description file according with VoID specifications and then learn all found datasets. This command is essentially the same as the combination of the following pseudo code:

	if sd learn resource -a PUT *SOURCE*/well-known/void; then
		for dataset in $(echo "SELECT DISTINCT ?dataset {  GRAPH <*SOURCE> {?dataset <http://rdfs.org/ns/void#dataDump> [] }}" | sd_sparql_list); do
			sd learn dataset "$dataset"
		done
	fi


*DATA_ROOT*
: the uri of a VOid home page. source must be an http home page ending with a /

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

*METADATA*
: following metadata are supported:

| Variable Name         | Default                         | Description                                                   |
|-----------------------|---------------------------------|---------------------------------------------------------------|
| sid                   | STORE                           | A sid name                                                    |
| trust                 | 1                               | Trust level as a xds:decimal ranging 0..1                     |
| activity              | Activity                        | a KEES activity type (without namespace) OR xsd:anyURI        |
| graph                 | same of source                  | XSD:anyURI Named graph name                                   |
| activity_id           | *uuid*                          | XSD:anyURI Identifier for a prov:Activity                     |
| started_at            | <now>                           | activity start date                                           | 

### Exit status
Exits with 0 on success, and > 0 if a curl or http error occurs. 

### Availability
Since SDaaS 4

### Example

sd learn data http://data.example.org/