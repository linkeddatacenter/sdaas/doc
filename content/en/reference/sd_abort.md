---
title: sd_abort
description: log a critical error and exit SDaaS
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_abort** [*MESSAGE*]

### Description

logs a *MESSAGE* for critical error and exit from the caller

### Exit status
Exits 0 on success, and 2 if an error occurs.

### Availability
Since SDaaS 4

### Example

`sd_abort "command failed"`
: abort the program logging the error message "command failed"


`<command> || sd_abort  "command failed"`
: abort the program if <command> returns not 0