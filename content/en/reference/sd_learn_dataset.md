---
title: ⚗️sd_learn_dataset
description: "loads all data dumps in a VOiD dataset"
module: learn
component: Learning
distribution:
    - Enterprise Edition
---

### Synopsis

**sd_learn_dataset**  [-s *SID*]  [-D *METADATA*] *DATASET*

### Description
Loads all dataset dumps file  according with VoID specifications.This command is essentially the same as the combination of the following pseudo code:

    for dd in $(echo "SELECT DISTINCT ?dd { <$graph}> void:dataDump ?dd }" | sd sparql list); do
        sd learn resource 
    done

*DATASET*
: the URI of a VOid Dataset to be load. Only datasets exposing data dumps are considered.

-s *SID*
: connect to Graph Store named *SID* ( `STORE` by default)

-D *METADATA*
: the following local variables are supported:

| Variable Name         | Default                         | Description                                                   |
|-----------------------|---------------------------------|---------------------------------------------------------------|
| sid                   | STORE                           | same as -s                                                    |
| trust                 | 1                               | Trust level as a xds:decimal ranging 0..1                     |
| activity              | Activity                        | a KEES activity type (without namespace) OR xsd:anyURI        |
| graph                 | same of source                  | XSD:anyURI Named graph name                                   |
| activity_id           | *uuid*                          | XSD:anyURI Identifier for a prov:Activity                     |
| started_at            | <now>                           | activity start date                                           | 
| accrual_policy        | POST                            | graph accrual policy: POST or PUT                             |


### Exit status
Exits with 0 on success, and > 0 if error occurs. 

### Availability
Since SDaaS 4

### Example

sd learn resource -D "graph=https://dbpedia.org/" https://dbpedia.org/resource/Milan

