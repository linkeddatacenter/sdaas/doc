---
title: sd_validate
description: validate a variable content against a regular expression
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_validate** *VARIABLE_NAME* *REGULAR_EXPRESSION** 

### Description

validate a variable content against a regular expression

### Exit status
Exits 0 on success, and > 0 if an error occurs.

### Examples

`sd_validate STORE "^http"`
: test that the content of the variable named `STORE` begins with `http`

### Availability
Since SDaaS 4