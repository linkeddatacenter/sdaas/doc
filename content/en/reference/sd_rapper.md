---
title: sd_rapper
description: a wrapper for rapper
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_rapper**  *ARGUMENTS*

### Description
It is an interface to the [raptor library](https://librdf.org/raptor/) that converts a RDF into the various serialization. It accepts the same options of the 
[rapper utility](https://librdf.org/raptor/rapper.html) having the following options as default:

```	    
    -wq 
    -f 'xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"' 
    -f 'xmlns:owl="http://www.w3.org/2002/07/owl#"' 
    -f 'xmlns:foaf="http://xmlns.com/foaf/0.1/"' 
    -f 'xmlns:dc="http://purl.org/dc/elements/1.1/"' 
    -f 'xmlns:dct="http://purl.org/dc/terms/"' 
    -f 'xmlns:dqv="http://www.w3.org/ns/dqv#"' 
    -f 'xmlns:prov="http://www.w3.org/ns/prov#"' 
    -f 'xmlns:sd="http://www.w3.org/ns/sparql-service-description#"' 
    -f 'xmlns:void="http://rdfs.org/ns/void#"' 
    -f 'xmlns:xsd="http://www.w3.org/2001/XMLSchema#"' 
    -f 'xmlns:kees="http://linkeddata.center/kees/v1#"' 
    -f 'xmlns:sdaas="http://linkeddata.center/sdaas/v4#"' 
```


### Exit status
Exits with 0 on success, and > 0 if a curl or http error occurs. In case of http error, all the curl output is logged. 

### Availability
Since SDaaS 4

### Examples

`sd_rapper -g https://schema.org/version/latest/schemaorg-current-http.rdf`
: get the XML resource file and writes ntriple using serialization guess


`sd_rapper -i turle https://schema.org/version/latest/schemaorg-current-http.ttl`
: get the ttl resource file and writes ntriple with explict serialziaztion type
