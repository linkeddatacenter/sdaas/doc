---
title: sd_store_size
description: return the number of triple un a graph store 
module: store
component: Query
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_store_size** [-s *SID*] [-D *LOCAL CONTEXT*]

### Description
return the number of triple un a graph store. Please note tha in some store engine, for performance reasons, this feature returns an estimated number

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
0 on success

### Availability
Since SDaaS 4


