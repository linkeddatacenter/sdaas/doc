---
title: sd_view_modules
description: display all modules in SDaaS distribution
module: view
component: Command
distribution:
    - Community Edition
    - Enterprise Edition
---
Displays all available modules


# Synopsys
sd view modules

Examples:
```bash
sd view modules
```