---
title: sd_log
description: log a message
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_log** [-f *FILE*] [-p *PRIORITY*]  [-S *SIZE*] *MESSAGE*

### Description

log *MESSAGE* on stderr

-f *FILE*
:  Log the contents of the specified *FILE*. This option cannot be combined with a command-line message.

-p *PRIORITY*
: log the *MESSAGE* with the specified *PRIORITY*. The priority may be specified numerically or as a mnemonic string (see table). The default is `NOTICE`.  Logs is printed only if its priority is less then the value in `SD_LOG_PRIORITY` (default 6) 

{{% pageinfo color="info" %}}
Priority levels :

| level | mnemonic | explanation |
| -------------- | ----------------- |-------------|
| 2              | CRITICAL          | Should be corrected immediately, but indicates failure in a primary system - fix CRITICAL problems before ALERT - an example is loss of primary ISP connection. |
| 3              | ERROR             | Non-urgent failures - these should be relayed to developers or admins; each item must be resolved within a given time. |
| 4              | WARNING           | Warning messages - not an error, but indicated that an error will occur if action is not taken, e.g. file system 85% full - each item must be resolved within a given time. |
| 5              | NOTICE            | Events that are unusual but not error conditions - might be summarized in an email to developers or admins to spot potential problems - no immediate action required. |
| 6              | INFORMATIONAL     | Normal operational messages - may be harvested for reporting, measuring throughput, etc. - no action required. |
| 7              | DEBUG             | Info is useful to developers for debugging the app, not useful during operations. |
{{% /pageinfo %}}

-S *SIZE*
: Sets the maximum permitted message size to *SIZE*. The default is 1KiB characters, which is the limit traditionally used and specified in RFC 3164. With RFC 5424, this limit has become flexible. A good assumption is that RFC 5424 receivers can at least process 4KiB messages.

### Exit status
Exits 0 on success, and >0 if an error occurs.

### Configuration variables

The following configuration variables shall affect the execution of `sd_log`:

SD_LOG_PRIORITY
: contains a number 2-7 that means the minimum message priority level that must be printed by default is 6 (INFORMATIONAL) 


### Availability
Since SDaaS 4

### Examples

`sd_log "test message"`
: write the string "sdaas(5) -  test message" on stdout only if SD_LOG_PRIORITY>=5

`sd_log -p DEBUG "test message"`
: write the string "sdaas(7) -  test message" on stdout only if SD_LOG_PRIORITY=7

`sd_log ERROR "test message"`
: write the string "sdaas(3) - test message" on stderr
