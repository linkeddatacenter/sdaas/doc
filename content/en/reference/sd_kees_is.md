---
title: sd_kees_is
module: kees
component: Query
distribution: enterprise edition
---

### Synopsis

**sd_kees_is** [-D "sid=*SID* ] [-s *SID*]  *STATUS*

### Description

Returns 0 if the knowledge graph has  is a date associated to *STATUS*


*STATUS*
: is one one of :

- ingested : means that the knowledge graph is ingested
- reasoned : means that the knowledge graph is reasoned
- enriched : means that the knowledge graph is enriched
- published : means that the knowledge graph is published
- versioned : means that the knowledge graph is versioned
- stable : means that the knowledge graph is published or versioned and not invalid
- invalid: means that there is a knowleghe graph invalidation request
- empty: means that the knowledge base is empty
- locked: means that the knowledge base is locked


-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)

### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable

### Exit status
Exits 0 on success, and > 0 if an error occurs.

### Availability
Since SDaaS 4 - Enterprise Edition

### Examples

