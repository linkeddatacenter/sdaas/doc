---
title: sd_curl_rdf
description: a wrapper for curl with content management for RDF
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_curl_rdf**  *ARGUMENTS*

### Description
It is an alias for `sd_curl -s -f -H "Accept: application/rdf+xml, text/turtle;q=0.9, application/n-triples;q=0.8, */*;q=0.5"`


### Exit status
Exits with 0 on success, and  not 0 if a curl or http error occurs.

### Availability
Since SDaaS 4
