---
title: sd_sparql_query
description: query a graph store with SPARQL
module: sparql
component: Query
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_sparql_query** [-D *LOCAL CONTEXT*] [-s *SID*]  [-o *OUT_FORMAT*] 

### Description
execute execute a SPARQL query construct from a SPARQL query string passed in stdin. 
It returns the results in stdout according the format specified in *OUT_FORMAT* (default `xml`). If the specified format is incompatible with the *QUERY_COMMAND* the selected format fallback to `xml`

All input and outputs must be in UTF8 charset.

-s *SID*, -D "sid=*SID*"
: connect to Graph Store named *SID* ( `STORE` by default)


-o *OUT_FORMAT* -D "out_format=*OUT_FORMAT*"
: write result in *OUT_FORMAT* 

*OUT_FORMAT*
: one of these

| Type              | Query Type          | Description                                              |
|-------------------|---------------------|----------------------------------------------------------|
| csv               | SELECT              |                                                          |
| csv-h             | SELECT              | Same as csv but without header                           |
| csv-1             | SELECT              | Same as csv-h, but only the first row is printed         |
| csv-f1            | SELECT              | Same as csv-1, but only the first field is printed       |
| boolean           | ASK                 | output "true" or "false"                                 |
| tsl               | SELECT              |                                                          |
| json              | SELECT              |                                                          |
| ntriples          | CONSTRUCT, DESCRIBE |                                                          |
| xml               | CONSTRUCT, DESCRIBE |                                                          |
| turtle            | CONSTRUCT, DESCRIBE |                                                          |
| rdfxml (default)  | CONSTRUCT, DESCRIBE |                                                          |
| test              | ASK, SELECT         | returns 0 if ASK returns true or SELECT results is empty |


### Context
The default context is loaded from the `SD_DEFAULT_CONTEXT` configuration variable


### Exit status
Exits 0 on success, -1 of test fail  and > 0 if an error occurs.

### Availability
Since SDaaS 4

### Examples

`cat 'SELECT DISTINCT ?class WHERE { ?s a ?class} LIMIT 10' | sd sparql query -o csv `
: execute a the sparql SELECT statement returning the result as a CSV stream


`cat 'SELECT DISTINCT ?class WHERE { ?s a ?class} LIMIT 10' | sd sparql query -D "out_format=csv"`
: same as above but using context definition



`cat  'ASK ?s WHERE {?s ?p ?o}' | sd sparql query -o boolean`
: echo "true" if ASK the statement is true


`cat'SELECT ?s WHERE { ?s a <urn:type> }' | sd sparql query -o test `
: return 0 if SELECT the statement returns an empty list