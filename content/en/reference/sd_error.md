---
title: sd_error
description: return an error code and log a message
module: core
component: SDaaS function
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_error** *MESSAGE* [*ERROR_CODE*]

### Description

logs a *MESSAGE* with ERROR priority

### Exit status
*ERROR_CODE* (1 by default)

### Availability
Since SDaaS 4

### Example

`<command> || return sd_error "command failed" 3 `
: return 3 if  the program if <command> returns not 0