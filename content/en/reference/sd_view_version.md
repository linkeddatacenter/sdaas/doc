---
title: sd_view_version
description: print the version of SDaaS interface
module: view
component: Command
distribution:
    - Community Edition
    - Enterprise Edition
---

### Synopsis

**sd_view_version**


### Description

print the version of SDaaS interface

### Exit status
Exits 0 on success, and >0 if an error occurs.

### Availability
Since SDaaS 4