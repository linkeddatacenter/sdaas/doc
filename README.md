![logo](https://linkeddata.center/resources/v4/logo/Logo-colori-trasp_oriz-640x220.png)

# SDaaS-EE Documentation Portal
This repository contains the documentation the SDaaS platform. The portal is devoted to developers and certified system integrators. All contents of this portal are protected by Copyright and **CAN NOT** be copied or distributed without written permission by a LinkedData.Center representative.

This site is published at [https://LinkedData.Center/sdaas](https://LinkedData.Center/sdaas). 


## Quickstart

run `docker-compose up --build`,  open your web browser and type `http://localhost:1313` in your navigation bar

## Credits
This site is developed using [Docsy](https://www.docsy.dev/), is deployed in [Netlify](https://www.netlify.com/) under e-artspace account 